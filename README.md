# Reset Button

## Requirements

- ESP8266 Board version 2.4.2
- ArduinoJson library

## Setup

Change SSID, Password and IP Addresses to match your network specifications.

``` c++
#define SSID "YourWiFiSSID"
#define WPA_KEY "yourWiFiPassword"

IPAddress local_ip(192,168,178,115);
IPAddress gateway(192,168,178,1);
IPAddress subnet(255,255,255,0);
```

and

``` c++
http.begin("192.168.178.51",8081, "/resetbutton");
```