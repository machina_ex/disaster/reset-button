#include <Arduino.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

#define SSID "YourWiFiSSID"
#define WPA_KEY "yourWiFiPassword"

IPAddress local_ip(192, 168, 178, 115);
IPAddress gateway(192, 168, 178, 1);
IPAddress subnet(255, 255, 255, 0);

#define REED_PIN D2
#define BUTTON_PIN D7

boolean reedContact = false;
boolean buttonPress = false;

void handleReedInterrupt()
{
    reedContact = true;
}

void handleButtonInterrupt()
{
    buttonPress = true;
}

uint64_t seconds = 0;
long milli_seconds = 0;

void setup()
{
    pinMode(D1, OUTPUT);
    digitalWrite(D1, HIGH);
    pinMode(REED_PIN, INPUT);
    pinMode(BUTTON_PIN, INPUT);
    pinMode(D4, OUTPUT);
    reedContact = digitalRead(REED_PIN);
    buttonPress = digitalRead(BUTTON_PIN);

    WiFi.config(local_ip, gateway, subnet);
    WiFi.begin(SSID, WPA_KEY);
    Serial.begin(115200);

    Serial.println("Connecting to WiFi..");
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    if (WiFi.status() == WL_CONNECTED)
    {
        Serial.println("Connected to the WiFi network");
    }

    WiFi.setAutoReconnect(true);

    attachInterrupt(digitalPinToInterrupt(REED_PIN), handleReedInterrupt, CHANGE);
    attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), handleButtonInterrupt, CHANGE);

    milli_seconds = millis();
    ESP.wdtEnable(WDTO_8S);
    Serial.print("start loop");
}

void loop()
{
    ESP.wdtFeed();
    if (millis() > milli_seconds + 1000 || millis() < milli_seconds)
    {
        digitalWrite(D4, LOW);
        seconds++;
        milli_seconds = millis();
        Serial.println(".");
        digitalWrite(D4, HIGH);
    }

    if ((reedContact || buttonPress) && WiFi.status() == WL_CONNECTED)
    { // Check WiFi connection status
        digitalWrite(D4, LOW);

        HTTPClient http;                                                          // Declare object of class HTTPClient
        http.begin("192.168.178.51", 8081, "/Tutorial/devices/http/ResetButton"); // Specify request destination

        StaticJsonBuffer<300> JSONbuffer; // Declaring static JSON buffer
        JsonObject &JSONencoder = JSONbuffer.createObject();

        delay(10);

        JSONencoder["button"] = digitalRead(BUTTON_PIN);
        JSONencoder["reed"] = digitalRead(REED_PIN);

        char JSONmessageBuffer[300] = {0};
        JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));

        http.addHeader("Content-Type", "application/json"); // Specify content-type header

        Serial.print(JSONmessageBuffer);

        int httpCode = http.POST(JSONmessageBuffer); // Send the request
        Serial.println(httpCode);

        http.end(); // Close connection

        digitalWrite(D4, HIGH);
        reedContact = false;
        buttonPress = false;
        seconds = 0;
    }

    if (seconds > 1200)
    {
        digitalWrite(D4, LOW);
        digitalWrite(D1, LOW);
    }
}
